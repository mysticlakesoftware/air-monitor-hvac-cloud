//======================================================================================================================
//functions/index.js

//2018 Copyright Mystic Lake Software

//This is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 3 of the License, or
//(at your option) any later version.

//This is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================

//Author: rmerriam

//======================================================================================================================

//  load the capabilities for the project

//  two tables are created. One is for the full set of data from an HVAC monitor, i.e 2 pressures, temps, etc.
//  the second is for the environmental monitoring of a single set of data 

//  load firebase and setup firestore
//  maintain order of 'functions' then 'admin' or things break


const functions = require('firebase-functions'); 
const admin = require('firebase-admin');

admin.initializeApp();
const firestore = admin.firestore();

const fire_hvac = admin.database().ref('/hvacstatus');  // double set of sensors
const fire_air = admin.database().ref('/airstatus');    // single set of sensors

// load bigquery
// setup the dataset (database) and tables
const BigQuery = require('@google-cloud/bigquery');
const bigquery = new BigQuery();
const dataset = bigquery.dataset("mystic_air_monitor");
const big_hvac = dataset.table("hvacstatus");
const big_air = dataset.table("airstatus");

// the pubsub capability to handle receiving the channel data
const PubSub = require('@google-cloud/pubsub');


/**
 * Cloud Function to be triggered by Pub/Sub that logs a message using the data
 * published to the topic.
 */
exports.airStatusPub = functions.pubsub.topic('air-status').onPublish( (message, event) => {

    console.log("v 0.22");  // used to see if new verion of code is running
                            // there is a time lag after uploading the new code and its activation
      
    // get the actual message data
    messageBody = Buffer.from(message.data, 'base64').toString();
//    console.log(messageBody);
    
    // break message into separate data items separated by spaces
    var res = messageBody.split(" ");

    if (res.length > 7) {   // is double set of sensors?
        const data = {
                timestamp: res[0],
                mac: res[1],        // mac address of Atom
                temp1: res[2],
                prs1: res[3],
                rh1: res[4],
                // co21: res[5],
                voc1: res[6],
                temp2: res[7],
                prs2: res[8],
                rh2: res[9],
                // co22: res[10],
                voc2: res[11]
        };

        return Promise.all([
            fire_hvac.push( data)
           , big_hvac.insert(data)
            ]);

    }
    else {  // only single set of sensors
        const data = {
                timestamp:res[0],
                mac: res[1],
                temp1: res[2],
                prs1: res[3],
                rh1: res[4],
                // co21: res[5],
                voc1: res[6],
        };

        return Promise.all([
            fire_air.push( data),
            big_air.insert(data)
            ]);

    }
});
