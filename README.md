Google Cloud Publication and Subscription
===
This directory contains the Firebase local setup for uploading publication and subscription code. See these sites for details:

* [Firebase Admin Setup](https://firebase.google.com/docs/admin/setup)
* [Firebase Functions Setup]( https://firebase.google.com/docs/functions/)
